################################################################
from __future__ import print_function
import sys
import argparse
from compute_pi import ComputePi
from compute_arithmetic import ComputeArithmetic
from print_series import PrintSeries
from plot_series import PlotSeries
################################################################


def main():

    parser = argparse.ArgumentParser(
        description='Series exercise in python')
    parser.add_argument('-s', '--series_type', type=str,
                        help=('pi or arithmetic'),
                        required=True)
    parser.add_argument('-d', '--dumper_type', type=str,
                        help=('print or plot'),
                        required=True)
    parser.add_argument('-n', '--maxiter', type=int, required=True,
                        help='number of loop iteration to compute the series')
    parser.add_argument('-f', '--frequency', type=int, default=1,
                        help='frequency at which dumps/plots are made')
    parser.add_argument('-o', '--outfile', type=str, default="",
                        help='file where to store the output')

    args = parser.parse_args()
    series_type = args.series_type
    dumper_type = args.dumper_type
    maxiter = args.maxiter
    freq = args.frequency
    outfile = args.outfile

    #################################################################
    # creation of ojects
    #################################################################

    if series_type == "pi":
        serie_object = ComputePi()
    elif series_type == "arithmetic":
        serie_object = ComputeArithmetic()
    else:
        raise RuntimeError("unknown series type: " + series_type)

    if dumper_type == "print":
        dumper_object = PrintSeries(serie_object, maxiter, freq)
    elif dumper_type == "plot":
        dumper_object = PlotSeries(serie_object, maxiter, freq)
    else:
        print("unknown dumper type: " + dumper_type)
        sys.exit(-1)

    dumper_object.setPrecision(20)
    dumper_object.setOutFile(outfile)

    #################################################################
    # start of the run
    #################################################################

    dumper_object.dump()


if __name__ == "__main__":
    main()

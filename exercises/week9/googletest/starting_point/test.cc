#include <gtest/gtest.h>
#include "csv_reader.hh"
#include "planet.hh"
#include "system.hh"
#include "planets_factory.hh"

TEST(test, IO_interface) {

    PlanetsFactory::getInstance();

    System system;
    Planet p1;
    p1.getMass() = 1.;
    //p1.getPosition() = 1.;
    //p1.getForce() = 1.;
    //std::shared_ptr<> p1_shr_ptr = p1;
    system.addParticle(std::make_shared<Planet>(p1));

    Planet p2;
    system.addParticle(std::make_shared<Planet>(p2));

    CsvReader reader("../circle_orbit.csv");

    reader.read(system);

    std::cout<<system.getParticle(0)<<std::endl;
    std::cout<<system.getParticle(1)<<std::endl;
    std::cout<<system.getParticle(2)<<std::endl;
    std::cout<<system.getParticle(3)<<std::endl;
    std::cout<<system.getParticle(4)<<std::endl;
    
}


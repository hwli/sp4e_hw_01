#include <iostream>
#include <vector>

template <typename Vec, typename T> void for_each(Vec &vec, T f) {
  for (auto &v : vec) {
    auto res = f(v);
  }
}

int main() {
  int a = 2;
  std::vector<double> v(10000);
  for_each(v, [&a](auto &v) { return a; });
}
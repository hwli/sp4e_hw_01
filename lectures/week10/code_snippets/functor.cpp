#include <iostream>
#include <map>

struct MyFunctor {
  int operator()() { return 2; }
};

int main() {
  auto f = MyFunctor();
  std::cout << f() << std::endl;
}
#include <iostream>
#include <map>

template <typename T> void loop(T f) {
  for (int i = 0; i < 100000; ++i) {
    auto res = f();
  }
}

int main() {
  int a = 2;
  loop([&a]() { return a; });
}
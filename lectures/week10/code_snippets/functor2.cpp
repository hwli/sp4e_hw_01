#include <iostream>
#include <map>

struct MyFunctor {
  inline int operator()() { return a; }
  int a;
};

int main() {
  auto f = MyFunctor();
  int a = 2;
  f.a = a;

  for (int i = 0; i < 100000; ++i) {
    auto res = f();
  }
}
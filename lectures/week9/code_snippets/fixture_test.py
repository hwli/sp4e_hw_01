
import pytest
import numpy as np

class Particle:
    def __init__(self):
        position = np.zeros(3)
        velocity = np.zeros(3)
        force = np.zeros(3)

        
        
class System:
    pass
        
        
@pytest.fixture
def SystemTest():
    # SetUp
    sys = System()
    sys.particles = [Particle() for i in range(0, 10)]
    yield sys 
    # TearDown
    sys.particles = []
    return 

def test_is_not_empty(SystemTest):
    assert len(SystemTest.particles) == 10  

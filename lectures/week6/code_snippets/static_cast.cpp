#include <iostream>

struct Base {};

struct A : public Base {

  int a = 1;
  double b = 3.14;
};

struct B : public Base {
  double a = 3.14;
  int b = 1;
};

int main() {

  A a;
  Base &base_ref = a;
  B &b_ref = static_cast<B &>(base_ref);
  std::cout << b_ref.a << std::endl;
}
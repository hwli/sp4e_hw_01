#include <iostream>
#include <sstream>

int main(int argc, char **argv) {

  std::stringstream sstr("toto 2.5");
  int arg1;
  double arg2;
  sstr >> arg1 >> arg2;

  // good state if no error occurred
  std::cout << "good: " << sstr.good() << std::endl;

  std::cout << arg1 << " " << arg2 << std::endl;
}